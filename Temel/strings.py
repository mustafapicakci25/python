# -*- coding: utf-8 -*-

##################################################
# SUBSTRİNG


Mesaj = "Merhaba Dünya"

print(Mesaj[2:5]) # 2 den 5 e kadar 5 dahil değil
YeniMesaj = Mesaj[2:5]
#print(Mesaj[2:])   2den itibaren kalanı verir.
#print([:2])        Baştan 2 ye kadar verir.

##################################################
# String- Len Fonksiyonu

print(len(Mesaj)) #len metnin uzunlugunu verir.
YeniMesaj2 = Mesaj[len(Mesaj)-1 : len(Mesaj)]
print(YeniMesaj2)

##################################################
# Lower - Upper

print(Mesaj.upper()) # büyük harfe çevirir.
print(Mesaj.lower()) # küçük harfe çevirir. 
##################################################
# Replace

print(Mesaj.replace("ü","u")) # ü harfi u harfi ile değiştirildi.

print(Mesaj.replace("a","e")) # a yerine e yazdırdık .
##################################################
# Split - Strip

#bilgi="Mustafa Pıçakçı 20 Muğla"
#print(bilgi.split()) # split bunu dizi, liste gibi düşünebilmemizi sağlıyor. Burada boşluğa kadarki kısımları dizi indexi gibi ayırıyor.

bilgi="Mustafa;Pıçakçı;20;Muğla"
print(bilgi.split(";")) #Burada ';' a göre ayırdı. 

#bilgi2="     Mustafa;Pıçakçı;20;Muğla  ".strip() #strip baş ve sondaki boşlukları atıyor.
#print(bilgi2)

print(bilgi.split(";")[2])
##################################################
# Input

#ad=input("Adınız : ")
sayi1 = input("sayi1 : ") #inputla alınan veriler metinsel ifadelerdir . Dolayısıyla alınan sayıları string olarak görüp yan yana topladı.
sayi2 = input("sayi2 : ")
print(sayi1+sayi2)
print(int(sayi1) + int(sayi2)) # sayıları toplayabilmemiz için tip dönüşümü yapmamız lazım.


