# -*- coding: utf-8 -*-

#ogrenci1 = "Mustafa"
#ogrenci2 = "Gökçe"
#ogrenci3 = "Melih"

ogrenciler=["Mustafa","Gokce","Melih"]
print(ogrenciler[1])
ogrenciler.append("Omer") #append listeye eleman eklemek için kullanılır.
ogrenciler.remove("Melih") # remove listeden eleman silmek için kullanılır. 
ogrenciler[0]="Ugur"
print(ogrenciler)

#List constructor
sehirler=list(("Ankara","İstanbul"))
print(sehirler)
print(len(sehirler))